# DevOps
### This repository contains the documents related to 

## sonaqube installation on ubuntu ## Ref link https://www.vultr.com/docs/install-sonarqube-on-ubuntu-20-04-lts/

## 1. Install OpenJDK 11 on Dev/QA/Prod Jenkins Server

sudo apt-get install openjdk-11-jdk -y

## sudo apt install maven ## install on Jenkins Server

java -version ## should have java 11

## 2. Install and Configure PostgreSQL

sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'

wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -

sudo apt install postgresql postgresql-contrib -y

sudo systemctl enable postgresql

sudo systemctl start postgresql

sudo passwd postgres ## Djay@2022

su - postgres

createuser sonar

psql

ALTER USER sonar WITH ENCRYPTED password 'Djay@2022';

CREATE DATABASE sonarqube OWNER sonar;

GRANT ALL PRIVILEGES ON DATABASE sonarqube to sonar;

\q

exit

## 3. Download and Install SonarQube

sudo wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-9.4.0.54424.zip

sudo unzip sonarqube-9.4.0.54424.zip

sudo mv sonarqube-9.4.0.54424 /opt/sonarqube

## 4. Add SonarQube Group and User

sudo groupadd sonar

sudo useradd -d /opt/sonarqube -g sonar sonar

sudo chown sonar:sonar /opt/sonarqube -R

## 5. Configure SonarQube

sudo vi /opt/sonarqube/conf/sonar.properties ## uncomment and change below lines 

## Below those two lines, add the sonar.jdbc.url.

## sonar.jdbc.username=sonar
## sonar.jdbc.password=Djay@2022
## sonar.jdbc.url=jdbc:postgresql://localhost:5432/sonarqube

sudo vi /opt/sonarqube/bin/linux-x86-64/sonar.sh ## About 50 lines down, locate this line & comment: #RUN_AS_USER=

## RUN_AS_USER=sonar

## 6. Setup Systemd service

sudo vi /etc/systemd/system/sonar.service


####

[Unit]
Description=SonarQube service
After=syslog.target network.target

[Service]
Type=forking

ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop

User=sonar
Group=sonar
Restart=always

LimitNOFILE=65536
LimitNPROC=4096

[Install]
WantedBy=multi-user.target

####

sudo systemctl enable sonar

sudo systemctl start sonar

sudo systemctl status sonar


## 7. Modify Kernel System Limits

sudo vi /etc/sysctl.conf 

## 
vm.max_map_count=262144
fs.file-max=65536
ulimit -n 65536
ulimit -u 4096
##

sudo reboot

## 8. Access SonarQube Web Interface

http://3.109.62.203:9000

## Log in with username admin and password admin. SonarQube will prompt you to change your password.
## password changed to Djay@7079